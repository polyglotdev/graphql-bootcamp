import { GraphQLServer } from 'graphql-yoga'

const courses = [
  {
    id: '1',
    title: 'Modern JavaScript Bootcamp',
    author: 'Andrew Mead',
    locationOfCourse: 'https://www.udemy.com',
    tags: ['JavaScript', 'GraphQL'],
    length: '23 hours',
    rating: '⭐⭐⭐⭐⭐'
  }
]

const typeDefs = `
  type Query {
    hello: String!
    courses: Courses!
  }

  type Courses {
    id: ID!
    title: String!
    author: String!
    locationOfCourse: String!
    tags: [String!]!
    length: String!
    rating: String!
  }
`

const resolvers = {
  Query: {
    hello(parent, args, ctx, info) {
      return `Hello, World`
    },
    courses(parent, args, ctx, info) {
      return {
        id: '1',
        title: 'Modern GraphQL Bootcamp',
        author: 'Andrew Mead',
        locationOfCourse: 'https://www.udemy.com/course/graphql-bootcamp/',
        tags: ['JavaScript', 'GraphQL'],
        length: '23 hours',
        rating: '⭐⭐⭐⭐⭐'
      }
    }
  }
}

const server = new GraphQLServer({ typeDefs, resolvers })

server.start(() => {
  console.log(`The 🚀 is running at:http://localhost:4000`)
})
