const users = [
  {
    id: '1',
    name: 'Dom',
    email: 'dom@example.com',
    userAge: 36,
    occupation: 'Software Engineer'
  },
  {
    id: '2',
    name: 'Becky',
    email: 'becky@example.com',
    userAge: 39,
    occupation: 'Teacher'
  },
  {
    id: '3',
    name: 'Elijah',
    email: 'elijah@example.com',
    userAge: 4,
    occupation: 'Child'
  }
]

const posts = [
  {
    id: '1',
    title: 'First post',
    body: 'This is my first post, and I am super excited',
    published: true,
    author: '1'
  },
  {
    id: '2',
    title: 'Becky Blog',
    body:
      'Hey, everyone! My name is Becky, and I would like to welcome you to my first blog post!',
    published: true,
    author: '2'
  },
  {
    id: '3',
    title: `Elijah's Blog`,
    body: 'I like trains!',
    published: true,
    author: '3'
  }
]

const comments = [
  {
    id: '1',
    text: 'Did I just comment on my own shit?? Yep!',
    author: '1',
    post: '1'
  },
  {
    id: '2',
    text: 'This is kinda boring',
    author: '2',
    post: '2'
  },
  {
    id: '3',
    text: 'Did I mention how much I love trains?',
    author: '3',
    post: '3'
  },
  {
    id: '4',
    text: 'Yes, Elijah you mentioned that... Literally every freaking day',
    author: '2',
    post: '3'
  }
]

const db = {
  users,
  posts,
  comments
}

export { db as default }
