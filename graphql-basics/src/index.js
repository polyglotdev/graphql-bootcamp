import Comment from './resolvers/Comment'
import db from './db'
import Mutation from './resolvers/Mutation'
import Post from './resolvers/Post'
import Query from './resolvers/Query'
import User from './resolvers/User'
import { GraphQLServer } from 'graphql-yoga'

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers: {
    Query,
    Mutation,
    User,
    Post,
    Comment
  },
  context: {
    db
  }
})

server.start(() => {
  console.log(`The server is running at http://localhost:4000/ 🚀`)
})
